impress().init();
$(document).ready(function(){
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                 $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                  $(window).scrollLeft()) + "px");
        return this;
    };

    $("span.projects").click(function(){
        var element = "#" +  $(this).data("project") + " div>ul";
        $(element)
        // .prepend($("span.button").show())
            .bPopup({
                appendTo: '#modal',
                onClose: function() {
                    $(this).removeAttr('style');
                }
            })
            ;
    });

    if ( $.browser.mozilla ) {
        $('#profile div.social').addClass('firefox-fix');
    }

    $( "#profile" ).bind( "impress:stepleave", function() {
        $('div.hint').remove();
    });
    $( "#overview" ).bind( "impress:stepenter", function() {
        $("div#extras").css('bottom', '1%');
    });
    $( "#overview" ).bind( "impress:stepleave", function() {
        $("div#extras").css('bottom', '-10%');
    });
});
